package WebService::Dota2::API;

use LWP::UserAgent;
use HTTP::Request;
use JSON qw/ decode_json encode_json /;
use Data::Dumper;
use Try::Tiny;

use WebService::Dota2::Cache;

our $API_MAP = {
    vanity_url              => { endpoint => 'ISteamUser/ResolveVanityURL/v0001/',      params => [ 'vanityurl' ],                          max_cache => 60 * 60 * 3    },
    heroes                  => { endpoint => 'IEconDOTA2_570/GetHeroes/v0001/',         params => [ 'language' ],                           max_cache => 86400 * 7  },
    get_player_summaries    => { endpoint => 'ISteamUser/GetPlayerSummaries/v0002/',    params => [ 'steamids' ],                           max_cache => 60 * 60 * 12    },
    match_history           => { endpoint => 'IDOTA2Match_570/GetMatchHistory/V001/',   params => [ 'account_id', 'matches_requested' ],    max_cache => 5 * 60    },
    match_details           => { endpoint => 'IDOTA2Match_570/GetMatchDetails/V001/',   params => [ 'match_id' ],                           max_cache => 86400 * 7 * 52  }, # ONE WHOLE YEAR
};

sub new {
    my ($class, $api_key, $cache_file) = @_;
    
    my $self = { api_key => $api_key };
    my $params = {};
    $cache_file = "dota2.db" unless $cache_file;
    $params->{dbname} = $cache_file;
    $self->{cache} = WebService::Dota2::Cache->new($params);
    
    return bless($self, __PACKAGE__);
}

sub json_decode {
    my ($self, $data) = @_;
    my $decoded;
    try {
        no warnings 'exiting';
        $decoded = decode_json($data);
    } catch {
        die "Unable to decode JSON: $_";
    };
    return $decoded;
}

sub cache {
    my $self = shift;
    return $self->{cache};
}

sub call {
    my ($self, $command, $params, $base) = @_;
    return unless $map = $API_MAP->{$command};
    
    my $endpoint = $map->{endpoint};
    my $query_params = $self->_gen_query_params($params);
    
    my $cache = $self->{cache};
    my $cached_response = $cache->retrieve({
        command => $command,
        params => $query_params,
    });
    #warn "Using cached response for $command:$query_params" if $cached_response;
    return $self->json_decode($cached_response) if $cached_response;
    print "Calling Steam API: $command:$query_params";
    my $query = '?key=' . $self->api_key . "&$query_params";
    my $ua = LWP::UserAgent->new;
    $ua->agent("$WebService::Dota2::AGENT/$WebService::Dota2::VERSION");
    $ua->timeout(10);
    
    my $res = $ua->get($WebService::Dota2::API . $endpoint . $query);
    if ($res->is_success) {
        my $c = $res->content;
        my $decoded = $self->json_decode($c);
        $cache->store({
            command => $command,
            params => $query_params,
            max_cache => $map->{max_cache},
            data => $c,
        });
        return $decoded;
    } else {
        #warn "Error code received: " . $res->status_line . "\n" if $ENV{EVE_DEBUG};
        return { error => $res->code, message => $res->message };    
    }
    return { error => 0, message => 'Unable to decode JSON' };
}

sub api_key {
    my $self = shift;
    return $self->{api_key};
}

sub _gen_query_params {
    my ($self, $params) = @_;
    return '' unless defined $params;
    my @query = ();
    foreach my $param (keys %{$params}) {
        my $value = $params->{$param};
        push(@query, "$param=$value");
    }
    return join('&', @query);
}
