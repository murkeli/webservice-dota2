package WebService::Dota2::Base;

use Data::Dumper;

use WebService::Dota2::API;

sub new {
    my ($class, $api_key, $cache_file) = @_;
    
    return bless({ api => WebService::Dota2::API->new($api_key, $cache_file) }, __PACKAGE__);
}

sub inherit {
    my $self = shift;
    return { api => $self->{api}, base => $self };
}

sub api {
    my $self = shift;
    return $self->{api};
}

1;
