package WebService::Dota2::Cache;

use strict;
use warnings;

our $VERSION = "0.61";

use DBI;
use Data::Dumper;

sub new {
    my ($class, $params) = @_;
    
    my $dbname = $params->{dbname};

    my ($handle, $sql);
    
    #if (not (-f $dbname)) {
    #    open(FH, $dbname) or die "Can't create $dbname: $!";
    #    close(FH);
    #}
    
    $handle = DBI->connect("dbi:SQLite:dbname=$dbname", "", "");
    die "Cannot create DB: $dbname" unless $handle;
    
    my $self = bless({ _handle => $handle, _dbname => $dbname }, __PACKAGE__);
    $self->initialize;

    $self->{_sql} = {
        store     	=> $handle->prepare("INSERT INTO dota2_cache (cachekey, cacheuntil, cachedata) VALUES (?, ?, ?)"),
        retrieve    => $handle->prepare("SELECT cachedata, cacheuntil FROM dota2_cache WHERE cachekey = ?"),
        delete    	=> $handle->prepare("DELETE FROM dota2_cache WHERE cachekey = ?"),
    };
    return $self;
}

sub initialize {
    my $self = shift;

    my $dbname = $self->{_dbname};
    my $handle = $self->{_handle};
    
    die "Problem initializing cache: $@" unless $handle;
    
    $handle->do("CREATE TABLE IF NOT EXISTS dota2_cache (cachekey VARCHAR(1024) NOT NULL PRIMARY KEY, cacheuntil INT NOT NULL, cachedata TEXT);");
}

sub store {
    my ($self, $details) = @_;
    
    my $cachekey = $details->{command} . ":" . $details->{params};
    my $cache_until = time + $details->{max_cache};
    
    # just to be safe, delete before insert
    $self->{_sql}->{delete}->execute($cachekey);
    $self->{_sql}->{store}->execute($cachekey, $cache_until, $details->{data});

    return $details->{data};
}

sub retrieve {
    my ($self, $details) = @_;
    
    my $data = undef;
    my $until = time - 1; # pretend cache has already expired
    my $now = time;
    
    my $cachekey =  $details->{command} . ":" . $details->{params};

    $self->{_sql}->{retrieve}->execute($cachekey);
    ($data, $until) = $self->{_sql}->{retrieve}->fetchrow;
    unless ($data && ($until >= $now)) {
        $self->{_sql}->{delete}->execute($cachekey);
        return undef;
    }
    # $data will only be returned if it exists and the cache on it hasn't expired
    return $data;
}

sub expire {
    my ($self, $cachekey) = shift;
    $self->{_sql}->{delete}->execute($cachekey);
}

1;