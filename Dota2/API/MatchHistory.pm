package WebService::Dota2::API::MatchHistory;
use strict;
use warnings;
use parent qw/ WebService::Dota2::Base /;

use Data::Dumper;

sub new {
    my ($class, $base) = @_;
    
    my $self = $base->inherit;
    return bless($self, __PACKAGE__);
}

sub details {
    my ($self, $mode) = @_;
    return WebService::Dota2::API::Match->new($self->{base}, $self->{match_id}, $mode);
}

sub latest_by_account_id {
    my ($self, $id) = @_;
    
    my $response = $self->api->call('match_history', { 'matches_requested' => 1, 'account_id' => $id });
    die "Error code received: $response->{error} $response->{message}" if $response->{error};
    die "Unknown match ID $id" unless $response->{result};
    my $result = $response->{result};
    die $result->{statusDetail} unless $result->{status} == 1;
    
    return bless({ %$self, %{$result->{matches}->[0]} }, __PACKAGE__);
}

1;