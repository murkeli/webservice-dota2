package WebService::Dota2::API::Profiles;
use strict;
use warnings;
use parent qw/ WebService::Dota2::Base /;

use Data::Dumper;

sub new {
    my ($class, $base) = @_;
    
    my $self = bless({ %{$base->inherit}, profiles => [] }, __PACKAGE__);
    return $self;
}

sub load {
    my ($self, $ids) = @_;
    $self->{profiles} = [];
    return $self unless $ids;
    
    my $response = $self->api->call('get_player_summaries', { 'steamids' => $ids }, $self);
    if ($response->{error}) {
        warn 'Error code received: ' . $response->{error} . ' ' . $response->{message};
        return $self;
    }
    my $profiles = $response->{response}->{players};
    my $profileObjects = [];
    for my $profile (@{$profiles}) {
        push(@$profileObjects, WebService::Dota2::API::Profile->new($self->{base}, 0, $profile));
    }
    $self->{profiles} = $profileObjects;
    return $self;
}

1;