package WebService::Dota2::API::Heroes;
use strict;
use warnings;
use parent qw/ WebService::Dota2::Base /;

use Data::Dumper;

sub new {
    my ($class, $base) = @_;
    
    my $self = bless($base->inherit, __PACKAGE__);
    return $self->_populate;
}

sub get_by_id {
    my ($self, $id) = @_;
    $self->_verify();
    return $self->{heroes}->{$id} if $self->{heroes}->{$id};
    return { id => 0, name => 'unknown', localized_name => '<Unknown hero>' };
}

sub get_all {
    my $self = shift;
    $self->_verify();
    return $self->{heroes};
}

sub _populate {
    my $self = shift;
    
    my $response = $self->api->call('heroes', { 'language' => 'en_us' }, $self);
    if ($response->{error}) {
        warn 'Error code received: ' . $response->{error} . ' ' . $response->{message};
        $self->api->cache->expire('heroes:language=en_us');
        return $self;
    }

    $self->{hero_count} = scalar(@{$response->{result}->{heroes}});
    $self->{heroes} = {};
    for my $hero (@{$response->{result}->{heroes}}) {
        $self->{heroes}->{$hero->{id}} = {
            id => $hero->{id},
            name => $hero->{name},
            localized_name => $hero->{localized_name},
        };
    }
    return $self;
}

sub _verify {
    my $self = shift;
    my $response = $self->api->call('heroes', { 'language' => 'en_us' }, $self);
    my $count = scalar(@{$response->{result}->{heroes}});
    if ($count != $self->{hero_count}) {
        $self->api->cache->expire('heroes:language=en_us');
        $self->_populate();
    }
}

1;
