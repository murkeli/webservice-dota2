package WebService::Dota2::API::Profile;
use strict;
use warnings;
use parent qw/ WebService::Dota2::Base /;

use Data::Dumper;

sub new {
    my ($class, $base, $id, $data) = @_;
    
    my $self = $base->inherit;
    if ($data) {
        $id = $data->{steamid};
        $self = { %$self, %$data };
    }
    $self = bless($self, __PACKAGE__);
    return $self if not $id;
    if ($id =~ /^\d+$/) {
        $self->set_id($id);
    } else {
        $self->resolve_vanity_url($id);
    }
    return $self;
}

sub set_id {
    my ($self, $id) = @_;
    return $self if $id == 4294967295;
    if (is32($id)) {
        $self->{steamid64} = to64($id);
        $self->{steamid32} = $id;
    } else {
        $self->{steamid64} = $id;
        $self->{steamid32} = to32($id);
    }
    return $self;
}

sub latest_match {
    my $self = shift;
    return WebService::Dota2::API::MatchHistory->new($self->{base})->latest_by_account_id($self->{steamid32});
}

sub load {
    my $self = shift;
    return $self unless $self->{steamid64};
    
    my $response = $self->api->call('get_player_summaries', { 'steamids' => $self->{steamid64} }, $self);
    if ($response->{error}) {
        warn 'Error code received: ' . $response->{error} . ' ' . $response->{message};
        return $self;
    }
    $response = $response->{response}->{players};
    return $self unless scalar @{$response};
    $self = bless({ %$self, %{$response->[0]} }, __PACKAGE__);
    return $self;
}

sub resolve_vanity_url {
    my ($self, $vanityurl) = @_;
    
    my $response = $self->api->call('vanity_url', { 'vanityurl' => $vanityurl }, $self);
    if ($response->{error}) {
        warn 'Error code received: ' . $response->{error} . ' ' . $response->{message};
        return $self;
    }
    $response = $response->{response};
    if ($response->{success} == 1) {
        $self->{vanity_url} = $vanityurl;
        $self->{steamid64} = $response->{steamid};
        $self->{steamid32} = to32($response->{steamid});
    }
    return $self;
}

sub is32 {
    my $id = shift;
    return $id < 4294967295
}

sub to32 {
    my $id = shift;
    return $id - 76561197960265728;
}

sub to64 {
    my $id = shift;
    return $id + 76561197960265728;
}

1;