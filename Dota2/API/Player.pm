package WebService::Dota2::API::Player;
use strict;
use warnings;
use parent qw/ WebService::Dota2::Base /;

use Data::Dumper;

sub new {
    my ($class, $base, $data) = @_;
    
    my $self = {};
    my $heroId = delete($data->{hero_id});
    $self->{hero} = $base->heroes->get_by_id($heroId);
    $self = bless({ %{$base->inherit}, %$self, %$data }, __PACKAGE__);
    return $self;
}

1;
