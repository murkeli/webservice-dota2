package WebService::Dota2::API::Match;
use strict;
use warnings;
use parent qw/ WebService::Dota2::Base /;

use Data::Dumper;

use WebService::Dota2::API::Player;

sub new {
    my ($class, $base, $id, $mode) = @_;
    
    my $self = bless($base->inherit, __PACKAGE__);
    $self = $self->load($id, $mode) if $id;
    return $self;
}

sub load {
    my ($self, $id, $mode) = @_;
    # mode defines whether we want to fetch data for player profiles, use 'full' if you need it
    $mode = 'minimal' if not $mode;
    my $response = $self->api->call('match_details', { 'match_id' => $id });
    if ($response->{error}) {
        warn 'Error code received: ' . $response->{error} . ' ' . $response->{message};
        return $self;
    } elsif (not $response->{result}) {
        warn "Unknown match ID: $id";
        return $self;
    }
    
    my $result = $response->{result};
    my $players = delete($result->{players});
    my $playerObjects = [];
    my @ids = ();
    for my $player (@{$players}) {
        # check if player has a profile ID (used for full mode)
        push(@ids, WebService::Dota2::API::Profile::to64($player->{account_id})) if defined $player->{account_id} and $player->{account_id} != 4294967295;
        push(@$playerObjects, WebService::Dota2::API::Player->new($self->{base}, $player));
    }
    if ($mode eq 'full') {
        my $profiles = WebService::Dota2::API::Profiles->new($self->{base})->load(join(',', @ids));
        for my $profile (@{$profiles->{profiles}}) {
            for my $player (@{$playerObjects}) {
                if ($player->{account_id} == $profile->{steamid32}) {
                    $player->{profile} = $profile;
                    last;
                }
            }
        }
    }
    $self = { %$self, %$result };
    $self->{players} = $playerObjects;
    return bless($self, __PACKAGE__);
}

1;
