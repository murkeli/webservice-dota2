# Overview
A simple perl5 implementation of a Dota2 API wrapper. Lacks features and isn't very smart but it's something. Uses SQLite to cache data.
# Example usage
	use Data::Dumper;
	use WebService::Dota2;

	my $dota = WebService::Dota2->new('api_key', 'cache_file_location'); # cache file defaults to "dota2.db"
	my $profile = $dota->get_profile('64bitId_or_32bitId_or_vanityName'); # vanity name requires an additional API call to resolve
	die('Could not get profile') unless $profile->{steamid64};
	my $match = $profile->latest_match;
	die('No matches found') unless $match->{match_id};
	my $details = $match->details;
	print Dumper($details);
