package WebService::Dota2;
use strict;
use warnings;
use feature 'state';

use parent qw/ WebService::Dota2::Base /;

use WebService::Dota2::API::Heroes;
use WebService::Dota2::API::Profile;
use WebService::Dota2::API::Profiles;
use WebService::Dota2::API::Match;
use WebService::Dota2::API::MatchHistory;

our $VERSION = '0.1';
our $AGENT = 'WebService::Dota2';
our $API = 'https://api.steampowered.com/';

sub new {
    my ($class, $api_key, $cache_file) = @_;

    die "Cannot instantiate without an api_key!\nPlease visit http://steamcommunity.com/dev/apikey if you still need to get one." unless $api_key;

    return bless(WebService::Dota2::Base->new($api_key, $cache_file), __PACKAGE__);
}

sub heroes {
    state $heroes = WebService::Dota2::API::Heroes->new(@_);
    return $heroes;
}

sub resolve_vanity_url {
    my ($base, $url) = @_;
    return WebService::Dota2::API::Profile->new($base)->resolve_vanity_url($url);
}

sub get_profile {
    return WebService::Dota2::API::Profile->new(@_);
}

sub get_profiles {
    my ($base, $ids) = @_;
    return WebService::Dota2::API::Profiles->new($base)->load($ids);
}

sub get_match_details {
    return WebService::Dota2::API::Match->new(@_);
}

sub match_history {
    return WebService::Dota2::API::MatchHistory->new(@_);
}

1;
